# libgit2 1.3.0-1

 - mingw-w64-i686-zlib-1.2.11-9100-any.pkg.tar.xz
 - mingw-w64-i686-openssl-1.1.1.m-9800-any.pkg.tar.xz
 - mingw-w64-i686-libssh2-1.10.0-9800-any.pkg.tar.xz
 - mingw-w64-i686-libgit2-1.3.0-1-any.pkg.tar.xz
 - mingw-w64-x86_64-zlib-1.2.11-9100-any.pkg.tar.xz
 - mingw-w64-x86_64-openssl-1.1.1.m-9800-any.pkg.tar.xz
 - mingw-w64-x86_64-libssh2-1.10.0-9800-any.pkg.tar.xz
 - mingw-w64-x86_64-libgit2-1.3.0-1-any.pkg.tar.xz
 - mingw-w64-ucrt-x86_64-zlib-1.2.11-9100-any.pkg.tar.xz
 - mingw-w64-ucrt-x86_64-openssl-1.1.1.m-9800-any.pkg.tar.xz
 - mingw-w64-ucrt-x86_64-libssh2-1.10.0-9800-any.pkg.tar.xz
 - mingw-w64-ucrt-x86_64-libgit2-1.3.0-1-any.pkg.tar.xz
